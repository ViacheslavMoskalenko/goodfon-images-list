# GoodFon images scrapper
The project is aimed to be a handy scrapper for the goodfon.ru website that hosts plenty of high-quality wallpapers.

# Installation
Make sure you have `pipenv` installed globally on your machine.
If that is not the case, then install it first:
```
pip install pipenv
```
Next install all the `pipenv` dependencies by typing the following while being in the project's root directory:
```
pipenv install
```

# Run
Make sure you've activated `pipenv` shell by typing the following in the project's root directory:
```
pipenv shell
```
Then run the main executor by typing the following from the project's root directory:
```
python run.py
```

# Next features
[ ] User proxies list to prevent ban from GoodFon

[ ] Retry a failed download with an automatic proxy switch 
