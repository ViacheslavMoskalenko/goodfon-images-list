from bs4 import BeautifulSoup
import requests


class PageScrapper:
    def __init__(self, url):
        self.url = url
        self.bs = BeautifulSoup(self.get_html(), 'lxml')

    def get_html(self):
        resp = requests.get(self.url)
        return '' if resp.status_code != 200 else resp.text

    def get_images(self):
        print('Handling pictures scrapping for a page {}'.format(self.url))
        image_tags = self.bs.select('.wallpapers__item__img')
        return list(map(lambda img: img.attrs.get('src'), image_tags))

    def __repr__(self):
        return 'Page scrapper for url: {}'.format(self.url)
