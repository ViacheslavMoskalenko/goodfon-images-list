from picture_fetcher import PictureFetcher
from page_scrapper import PageScrapper
from multiprocessing import Pool

from utils import flat_list

BASE_URL = 'https://www.goodfon.ru'
PICTURES_LIST = 'pictures-list'
FIRST_PAGES_TO_SCRAP = 100


def store_pictures_list(pictures=None):
    print('Storing a pictures in the pictures-list file')
    if pictures is None:
        pictures = []
    with open(PICTURES_LIST, 'wt') as file:
        file.write('\n'.join(sorted(pictures)))


def get_page_images(page_path):
    return PageScrapper(BASE_URL + page_path).get_images()


def get_pictures_for_all_pages(pages_to_process):
    with Pool(processes=10) as pool:
        output = flat_list(pool.map(
            get_page_images,
            pages_to_process
        ))
        pool.close()
        pool.join()
    return output


def download_picture(preview_url):
    fetcher = PictureFetcher(preview_url)
    fetcher.load_sized_version('1920x1080')


def download_all_pictures(img_previews):
    with Pool(processes=10) as pool:
        pool.map(
            download_picture,
            img_previews
        )
        pool.close()
        pool.join()


def main():
    pages_count = FIRST_PAGES_TO_SCRAP
    pages_to_process = ['/index-{}.html'.format(i) for i in range(1, pages_count + 1)]
    pictures = get_pictures_for_all_pages(pages_to_process)
    store_pictures_list(pictures)
    download_all_pictures(pictures)
    print('Done')


if __name__ == '__main__':
    main()
