import re


def flat_list(items=None):
    if items is None:
        items = []
    return [x for sub_list in items for x in sub_list]


def get_original_from_preview(img_preview_url, size='1920x1080'):
    return re.sub(r'/wallpaper/[a-z\d]{1,6}/', '/original/{}/'.format(size), img_preview_url)


def get_img_name_from_url(img_url):
    matches = re.findall(r'[\da-z_-]+\.jpe?g|png', img_url)
    return matches[0] if matches != () else ''
