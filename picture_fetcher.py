import os

import requests

from utils import get_original_from_preview, get_img_name_from_url


class PictureFetcher:
    def __init__(self, preview_url):
        self.preview_url = preview_url

    def load_sized_version(self, size='1920x1080'):
        size_specific_url = get_original_from_preview(self.preview_url, size)
        img_name = get_img_name_from_url(size_specific_url)
        img_file_path = self._provision_output_path(img_name)
        print('Processing {}'.format(size_specific_url))
        img_data = self._get_img_content(size_specific_url)
        if img_data is None:
            print('Unable to download {}'.format(size_specific_url))
            return
        self.store(img_file_path, img_data)

    @staticmethod
    def _provision_output_path(img_name):
        img_file_path = 'images/{}'.format(img_name)
        os.makedirs(os.path.dirname(img_file_path), exist_ok=True)
        return img_file_path

    @staticmethod
    def _get_img_content(img_url):
        resp = requests.get(img_url)
        if resp.status_code != 200:
            return None
        return resp.content

    @staticmethod
    def store(img_file_path, img_data):
        with open(img_file_path, 'wb') as file:
            file.write(img_data)
